import React from 'react';
import Card from 'react-bootstrap/Card';

import Aux from '../../../hoc/Auxiliary';

const header = props => (
  <Aux>
    {/* Page header */}
    <Card>
      <Card.Header className={'text-center'}>
        <h3>
          {props.ironmanIcon} {props.title} Highscores
        </h3>
      </Card.Header>
    </Card>

    {/* Skill buttons */}
    <Card bg="secondary" className={'icon-menu'}>
      <Card.Body className={'p-2 text-center'}>{props.skillIcons}</Card.Body>
    </Card>
  </Aux>
);

export default header;
