import React from 'react';
import Card from 'react-bootstrap/Card';

import Aux from '../../../hoc/Auxiliary';

const header = props => (
  <Aux>
    {/* Skill buttons */}
    <Card bg="secondary" className={'icon-menu'}>
      <Card.Body className={'p-2 text-center'}>{props.skillIcons}</Card.Body>
    </Card>
  </Aux>
);

export default header;
