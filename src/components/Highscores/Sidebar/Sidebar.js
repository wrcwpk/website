import React from 'react';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import ProgressBar from 'react-bootstrap/ProgressBar';
import ListGroup from 'react-bootstrap/ListGroup';

import Aux from '../../../hoc/Auxiliary';
import FilterButton from './FilterButton';

import {
  calculateCombatLevel,
  calculateProgression,
  getValidStats,
  getValidBosses,
} from '../../../utils/highscores';

import {
  DAY_IN_SECONDS,
  WEEK_IN_SECONDS,
  MONTH_IN_SECONDS,
  HIGHSCORES,
  TRACKER,
  PLAYER,
  COMPARE,
  NO_RESP,
} from '../../../utils/constants.js';

// Generates additional compare input if required
function generateCompareInput(props) {
  if (props.mode !== PLAYER) {
    return (
      <Aux>
        <Form.Control
          onChange={props.playerOnChange}
          minLength="2"
          maxLength="12"
          type="text"
          name="searchPlayerOne"
          placeholder="Player 1"
          value={props.searchPlayerOne}
          style={{ boxShadow: 'none' }}
          required
        />
      </Aux>
    );
  }
}

function generateTopCard(props) {
  const prog = calculateProgression(props.data);
  switch (props.mode) {
    case HIGHSCORES:
    case TRACKER:
    case NO_RESP:
      return (
        <Aux>
          <Card.Header>Filter</Card.Header>
          <Card.Body>
            {/* Tracker/highscores buttons */}
            {
              <ButtonGroup className={'mb-2 d-block'}>
                <FilterButton
                  clickEvent={props.xpTrackerOnClick}
                  value={-1}
                  active={props.mode === 'highscores'}
                >
                  Highscores
                </FilterButton>
                <FilterButton
                  clickEvent={props.xpTrackerOnClick}
                  value={86400}
                  active={props.mode === 'tracker'}
                  disabled={
                    getValidStats().includes(props.section) ||
                    getValidBosses().includes(props.section)
                  }
                >
                  Tracker
                </FilterButton>
              </ButtonGroup>
            }

            {/* Regular/Ironman/UIM buttons */}
            <ButtonGroup className={'d-block'}>
              <FilterButton
                clickEvent={props.ironmanOnClick}
                value={0}
                active={props.iron === 0}
              >
                Regular
              </FilterButton>
              <FilterButton
                clickEvent={props.ironmanOnClick}
                value={1}
                active={props.iron === 1}
              >
                Ironman
              </FilterButton>
              <FilterButton
                clickEvent={props.ironmanOnClick}
                value={3}
                active={props.iron === 3}
              >
                UIM
              </FilterButton>
            </ButtonGroup>
          </Card.Body>
        </Aux>
      );
    case COMPARE:
      return (
        <Aux>
          <Card.Header>Compare by</Card.Header>
          <Card.Body>
            <ButtonGroup className="flex-wrap justify-content-center">
              <FilterButton
                clickEvent={props.compareFilterOnClick}
                value="experience"
                active={props.compareFilter === 'experience'}
              >
                Experience
              </FilterButton>
              <FilterButton
                clickEvent={props.compareFilterOnClick}
                value="level"
                active={props.compareFilter === 'level'}
              >
                Level
              </FilterButton>
            </ButtonGroup>
          </Card.Body>
        </Aux>
      );
    case PLAYER:
      return (
        <Aux>
          <Card.Header>Other Info</Card.Header>
          <ListGroup>
            <ListGroup.Item style={{ textAlign: 'left' }}>
              <strong>Quest Points:</strong>{' '}
              {props.data ? props.data['quest_points'] : '0'}
            </ListGroup.Item>
            <ListGroup.Item style={{ textAlign: 'left' }}>
              <strong>Combat Level:</strong> {calculateCombatLevel(props.data)}
            </ListGroup.Item>
            <ListGroup.Item>
              <p style={{ textAlign: 'left' }}>
                Level Completion: <strong>{prog.lvl}%</strong>
              </p>
              <ProgressBar
                style={{ borderRadius: '0' }}
                now={prog.lvl}
                variant={prog.lvl === 100 ? 'success' : 'primary'}
              />
            </ListGroup.Item>
            <ListGroup.Item>
              <p style={{ textAlign: 'left' }}>
                EXP Completion: <strong>{prog.exp}%</strong>
              </p>
              <ProgressBar
                style={{ borderRadius: '0' }}
                now={prog.exp}
                variant={prog.exp === 100 ? 'success' : 'primary'}
              />
            </ListGroup.Item>
          </ListGroup>
        </Aux>
      );
    default:
      return;
  }
}

const sidebar = props => {
  const isSortedDay = props.currentTimeFrame === DAY_IN_SECONDS;
  const isSortedWeek = props.currentTimeFrame === WEEK_IN_SECONDS;
  const isSortedMonth = props.currentTimeFrame === MONTH_IN_SECONDS;
  return (
    <Aux>
      {/*Top Card (Filters and Other Info*/}
      <Card bg="light" border="secondary" className={'mb-2 text-center'}>
        {generateTopCard(props)}
      </Card>

      {/* xp gain */}
      {(props.mode === PLAYER || props.mode === TRACKER) && (
        <Card color="light" className={'border-secondary mb-2 text-center'}>
          <Card.Header>XP Gain</Card.Header>
          <Card.Body>
            <Form.Group>
              <Button
                onClick={() => props.xpGainTimeChange(DAY_IN_SECONDS)}
                active={isSortedDay}
                type="submit"
                block
              >
                Day
              </Button>
              <Button
                onClick={() => props.xpGainTimeChange(WEEK_IN_SECONDS)}
                active={isSortedWeek}
                type="submit"
                block
              >
                Week
              </Button>
              <Button
                onClick={() => props.xpGainTimeChange(MONTH_IN_SECONDS)}
                active={isSortedMonth}
                type="submit"
                block
              >
                Month
              </Button>
            </Form.Group>
          </Card.Body>
        </Card>
      )}

      {/* Player search */}
      <Card bg="light" border="secondary" className={'mb-2 text-center'}>
        <Card.Header>Search</Card.Header>
        <Card.Body>
          <Form onSubmit={props.playerOnSubmit}>
            <Form.Group>
              <Form.Control
                onChange={props.playerOnChange}
                minLength="2"
                maxLength="12"
                name="searchPlayer"
                placeholder="Player"
                value={props.searchPlayer}
                style={{ boxShadow: 'none' }}
                required
              />
            </Form.Group>
            <Button variant="primary" type="submit" block>
              Search
            </Button>
          </Form>
        </Card.Body>
      </Card>

      {/* Player compare */}
      <Card color="light" className={'border-secondary mb-2 text-center'}>
        <Card.Header>Compare</Card.Header>
        <Card.Body>
          <Form onSubmit={props.compareOnSubmit}>
            <Form.Group>
              {generateCompareInput(props)}

              <Form.Control
                onChange={props.playerOnChange}
                minLength="2"
                maxLength="12"
                type="text"
                name="searchPlayerTwo"
                placeholder="Player 2"
                value={props.searchPlayerTwo}
                style={{ boxShadow: 'none' }}
                required
              />
            </Form.Group>
            <Button color="primary" type="submit" block>
              Compare
            </Button>
          </Form>
        </Card.Body>
      </Card>
    </Aux>
  );
};

export default sidebar;
