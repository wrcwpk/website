import React from 'react';

import {
  getIronmanIcon,
  formatPlayerName,
  formatNumber,
} from '../../../utils/highscores';

import ResponsiveTable from '../../UI/ResponsiveTable';

function generateHeader() {
  return (
    <tr>
      <th className={'text-right'} style={{ width: '70px' }}>
        Rank
      </th>
      <th className={'text-center'}>Username</th>
      <th className={'text-center'}>Kills</th>
    </tr>
  );
}

function generateBody(click, stat, page, data) {
  return data.map((p, k) => (
    <tr
      onClick={event => click(event, p.username)}
      key={p.username}
      id={p.username}
      style={{ cursor: 'pointer' }}
    >
      <td className={'text-right'}>{formatNumber(p[stat + '_rank'])}</td>
      <td className={'text-center'}>
        {getIronmanIcon(p.ironman)} {formatPlayerName(p.username)}
      </td>
      <td className={'text-center'}>{formatNumber(p[stat])}</td>
    </tr>
  ));
}

const bossTable = props => (
  <ResponsiveTable
    header={generateHeader()}
    body={generateBody(props.clickEvent, props.stat, props.page, props.data)}
  />
);

export default bossTable;
