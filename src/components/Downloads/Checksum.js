import React from 'react';

const checksum = props => (
  <p>
    <b>{props.title}</b>:{' '}
    <span style={{ backgroundColor: 'gray' }}>{props.checksum}</span>
  </p>
);

export default checksum;
