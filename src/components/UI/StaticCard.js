import React from 'react';
import Card from 'react-bootstrap/Card';

const authenticatorCard = props => (
  <Card className={'mb-2'}>
    <Card.Header className={`p-1 ${props.headerClasses}`}>
      {props.title}
    </Card.Header>
    <Card.Body className={`${props.bodyClasses}`}>{props.children}</Card.Body>
  </Card>
);
export default authenticatorCard;
