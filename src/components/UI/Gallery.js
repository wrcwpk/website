import React, { Component } from 'react';
import Lightbox from 'react-images';

import Aux from '../../hoc/Auxiliary';

const galleryContainer = {
  display: 'flex',
  flexWrap: 'wrap',
  alignItems: 'center',
  justifyContent: 'center',
  fontSize: 0,
};

const imgStyle = {
  margin: '.5vw',
  maxWidth: '25vw',
};

export default class Gallery extends Component {
  state = {
    lightboxIsOpen: false,
    currentImage: 0,
  };

  gotoPrevious = () => {
    this.setState({ currentImage: this.state.currentImage - 1 });
  };

  gotoNext = () => {
    this.setState({ currentImage: this.state.currentImage + 1 });
  };

  closeLightbox = () => {
    this.setState({ lightboxIsOpen: false });
  };

  openLightbox = imgIndex => {
    this.setState({ lightboxIsOpen: true, currentImage: imgIndex });
  };

  render() {
    return (
      <Aux>
        <div style={galleryContainer}>
          {this.props.images.map((img, i) => (
            <img
              onClick={() => this.openLightbox(i)}
              src={img.src}
              alt={'alt'}
              style={imgStyle}
              key={'img-' + i}
            />
          ))}
        </div>

        <Lightbox
          currentImage={this.state.currentImage}
          images={this.props.images}
          isOpen={this.state.lightboxIsOpen}
          onClickPrev={this.gotoPrevious}
          onClickNext={this.gotoNext}
          onClose={this.closeLightbox}
          showImageCount={false}
        />
      </Aux>
    );
  }
}
