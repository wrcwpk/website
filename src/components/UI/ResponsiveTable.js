import React from 'react';
import Table from 'react-bootstrap/Table';

const responseTable = props => (
  <Table size="sm" responsive striped hover bordered>
    <thead className={'bg-primary'}>{props.header}</thead>

    <tbody>{props.body}</tbody>
  </Table>
);

export default responseTable;
