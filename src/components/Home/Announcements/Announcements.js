import React, { Component } from 'react';

import StaticCard from '../../UI/StaticCard';
import CollapsableCard from '../../UI/CollapsableCard';
import MarkdownRender from '../../UI/MarkdownRender';

export default class Announcements extends Component {
  state = {
    toggle: [true, false, false, false, false],
  };

  toggle = propID => {
    //Split out the index of the prop by its id
    const idx = propID.split('-')[1];

    //Mutate the toggle array locally
    const toggleStates = this.state.toggle;
    toggleStates[idx] = !toggleStates[idx];

    //Set the state to the new array
    this.setState({ toggle: toggleStates });
  };

  generateAnnouncementBody(ann, i) {
    return ann.map((n, index) => {
      return <MarkdownRender key={`ann-${i}-${index}`} markdown={n} />;
    });
  }

  generateCards(announcements) {
    if (announcements === '') {
      return;
    }
    return announcements.map((a, i) => (
      <CollapsableCard
        toggle={this.toggle}
        isOpen={this.state.toggle[i]}
        title={a.title}
        body={this.generateAnnouncementBody(a.body, i)}
        id={`ann-${i}`}
        key={`ann-${i}`}
      />
    ));
  }

  render() {
    return (
      <StaticCard
        title={<strong>Announcements</strong>}
        headerClasses={'text-center'}
      >
        {this.generateCards(this.props.announcements)}
        <center>
          <p>Paypal donations can be sent to odelvidyascape@gmail.com</p>
        </center>
      </StaticCard>
    );
  }
}
