import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './assets/bootstrap-darkly.min.css';
import '@fortawesome/fontawesome-free/css/all.min.css';

ReactDOM.render(<App />, document.getElementById('root'));
